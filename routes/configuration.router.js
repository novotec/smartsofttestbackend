const { Router } = require('express')
const router = Router()

// Controllers
const { getTables, getTableDetail, getData, postData, putData, deleteData } = require('../controllers/configuration.controller')

// Routes Requests
router.get('/getTables', getTables)
router.get('/getTable/:id', getTableDetail)
router.get('/getData/:id', getData)
router.post('/data/:id', postData)
router.put('/data/:idTable/:id', putData)
router.delete('/data/:idTable/:id', deleteData)

module.exports = router
