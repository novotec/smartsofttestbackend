const { DataTypes } = require('sequelize')
const dbConnection = require('../database/config')

const TableModel = dbConnection.define('TableModel', {
  name: {
    type: DataTypes.STRING
  }
},
{
  tableName: 'typetable',
  timestamps: false
})

const StructureModel = dbConnection.define('StructureModel', {
  header: {
    type: DataTypes.STRING
  },
  datatype: {
    type: DataTypes.ENUM('date', 'string', 'number')
  },
  format: {
    type: DataTypes.STRING
  },
  required: {
    type: DataTypes.BOOLEAN
  }
},
{
  tableName: 'tablestructure',
  timestamps: false
})

const DataTable1 = dbConnection.define('DataTable1', {
  T1C1: {
    type: DataTypes.NUMBER
  },
  T1C2: {
    type: DataTypes.STRING
  },
  T1C3: {
    type: DataTypes.NUMBER
  },
  T1C4: {
    type: DataTypes.DATE
  }
},
{
  tableName: 'tabledata1',
  timestamps: false
})

const DataTable2 = dbConnection.define('DataTable2', {
  T2C1: {
    type: DataTypes.NUMBER
  },
  T2C2: {
    type: DataTypes.STRING
  },
  T2C3: {
    type: DataTypes.NUMBER
  },
  T2C4: {
    type: DataTypes.DATE
  },
  T2C5: {
    type: DataTypes.NUMBER
  }
},
{
  tableName: 'tabledata2',
  timestamps: false
})

const DataTable3 = dbConnection.define('DataTable3', {
  T3C1: {
    type: DataTypes.NUMBER
  },
  T3C2: {
    type: DataTypes.STRING
  },
  T3C3: {
    type: DataTypes.DATE
  },
},
{
  tableName: 'tabledata3',
  timestamps: false
})

module.exports = { TableModel, StructureModel, DataTable1, DataTable2 }
