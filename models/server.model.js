const express = require('express')
const cors = require('cors')
const morgan = require('morgan')

const dbConnection = require('../database/config')

class Server {
  constructor () {
    this.app = express()
    this.port = process.env.PORT
    this.pathConfiguration = '/api/configuration'

    this.connectionsDB()
    this.middleware()
    this.routes()
  }

  // Conexion DB
  async connectionsDB () {
    try {
      await dbConnection.authenticate()
      console.log('Db is connect')
    } catch (error) {
      throw new Error(error)
    }
  }

  // MIddleware
  middleware () {
    this.app.use(cors())
    this.app.use(express.json())
    this.app.use(morgan('dev'))
  }

  // Routes
  routes () {
    this.app.use(this.pathConfiguration, require('../routes/configuration.router'))
  }

  startServer () {
    this.app.listen(this.port, () =>
      console.log(`Server listening ${this.port}`)
    )
  }
}

module.exports = Server
