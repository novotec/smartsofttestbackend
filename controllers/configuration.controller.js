const { TableModel, StructureModel, DataTable1, DataTable2 } = require('../models/type-tables.model')

//Process to Get Data List
const getTables = async (req, res) => {
  const tables = await TableModel.findAll()
  res.json(tables)
}


//Methos fot load information detail
const getTableDetail = async (req, res) => {
  const { id } = req.params

  const table = await TableModel.findByPk(id)
  if (!table) {
    res.status(404).json({ msg: `No existe tabla con el id ${id}` })
  }
  const structure = await StructureModel.findAll({
    where: {
      tabletypeid: id
    }
  })

  res.json(
    [{ id: table.id, name: table.name, columns: structure }]
  )
}

const getData = async (req, res) => {
  const { id } = req.params
  let data = ''

  //Conditional for table id
  switch (id) {
    case '1':
      data = await DataTable1.findAll()
      res.json(data)
      break
    case '2':
      data = await DataTable2.findAll()
      res.json(data)
      break
    case '3':
      data = await DataTable1.findAll()
      res.json(data)
      break

    default:
      res.status(404).json({ msg: `No existe tabla con el id ${id}` })
      break
  }
}

//Method for Create
const postData = async (req, res) => {
  const { id } = req.params
  const { body } = req
  try {
    let data = ''
    switch (id) {
      case '1':
        data = new DataTable1(body)
        await data.save()

        res.json(data)
        break

      case '2':
        data = new DataTable2(body)
        await data.save()
        res.json(data)
        break

      default:
        res.status(404).json({ msg: `No existe tabla con el id ${id}` })
        break
    }
  } catch (error) {

  }
}

//Method for Update
const putData = async (req, res) => {
  const { idTable, id } = req.params
  const { body } = req
  try {
    let data = ''
    switch (idTable) {
      case '1':
        data = await DataTable1.findByPk(id)
        if (!data) {
          return res.status(404).json({ msg: `No existe tabla con el id ${id}` })
        }
        await data.update(body)
        res.json(data)

        break
      case '2':
        data = await DataTable2.findByPk(id)
        if (!data) {
          return res.status(404).json({ msg: `No existe tabla con el id ${id}` })
        }
        await data.update(body)
        res.json(data)

        break

      default:
        res.status(404).json({ msg: `No existe tabla con el id ${id}` })
        break
    }
  } catch (error) {

  }
}

//Mthod for delte rows
const deleteData = async (req, res) => {
  const { idTable, id } = req.params
  try {
    let data = ''

    //Condiciontal for table id
    switch (idTable) {
      case '1':
        data = await DataTable1.findByPk(id)
        if (!data) {
          res.status(404).json({ msg: `No existe tabla con el id ${id}` })
        }
        await data.destroy()
        res.json(data)
        break

      case '2':
        data = await DataTable2.findByPk(id)
        if (!data) {
          res.status(404).json({ msg: `No existe tabla con el id ${id}` })
        }
        await data.destroy()
        res.json(data)

        break

      default:
        res.status(404).json({ msg: `No existe tabla con el id ${id}` })
        break
    }
  } catch (error) {

  }
}

module.exports = { getTables, getTableDetail, getData, postData, putData, deleteData }
