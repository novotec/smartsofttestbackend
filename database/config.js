const { Sequelize } = require('sequelize')

const dbConnection = new Sequelize('smartsoft_joynner', 'root', '', {
  host: 'localhost',
  dialect: 'mysql'
})

module.exports = dbConnection
